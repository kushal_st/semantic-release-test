# [1.8.0](https://gitlab.com/kushal_st/semantic-release-test/compare/v1.7.0...v1.8.0) (2022-01-28)


### Bug Fixes

* removed code X ([72d3603](https://gitlab.com/kushal_st/semantic-release-test/commit/72d360377cca3f069893c7d2ce2f8e341b4819c6))


### Features

* added feature B ([a351a3d](https://gitlab.com/kushal_st/semantic-release-test/commit/a351a3d229aaad1b85bc0f6c73676d2b48532ecd))

# [1.7.0](https://gitlab.com/kushal_st/semantic-release-test/compare/v1.6.1...v1.7.0) (2022-01-28)


### Features

* **index-2.js:** added feature A ([a087702](https://gitlab.com/kushal_st/semantic-release-test/commit/a087702248f7832255e712064ae8b8f43ad0847b))

## [1.6.1](https://gitlab.com/kushal_st/semantic-release-test/compare/v1.6.0...v1.6.1) (2022-01-24)


### Bug Fixes

* index ([0a8dc66](https://gitlab.com/kushal_st/semantic-release-test/commit/0a8dc6659ca861798aff9fa33a916516ba69ea9c))

# [1.6.0](https://gitlab.com/kushal_st/semantic-release-test/compare/v1.5.2...v1.6.0) (2022-01-24)


### Bug Fixes

* conflicts ([d588eeb](https://gitlab.com/kushal_st/semantic-release-test/commit/d588eebbc44ef89d24c71e815db9d5a65c3ea857))
* index ([204924a](https://gitlab.com/kushal_st/semantic-release-test/commit/204924a91c5a4743e8149a0333c6a72ef9c527a8))
* linters ([0a371f0](https://gitlab.com/kushal_st/semantic-release-test/commit/0a371f07e7d9a19e60a2662ff95ef1c91cfb6644))


### Features

* adding linters ([d48b666](https://gitlab.com/kushal_st/semantic-release-test/commit/d48b666b146f96a75af92b192c7253f62287d056))

## [1.5.2](https://gitlab.com/kushal_st/semantic-release-test/compare/v1.5.1...v1.5.2) (2022-01-10)


### Bug Fixes

* removed extra file ([8dd77e3](https://gitlab.com/kushal_st/semantic-release-test/commit/8dd77e3b90db952c63f5916ac252535deeff22cb))

## [1.5.1](https://gitlab.com/kushal_st/semantic-release-test/compare/v1.5.0...v1.5.1) (2022-01-10)


### Bug Fixes

* releaserc ([5cb5c37](https://gitlab.com/kushal_st/semantic-release-test/commit/5cb5c373b324e673b927d8882680f27afbac6dab))
* removing semantic npm ([fad69c6](https://gitlab.com/kushal_st/semantic-release-test/commit/fad69c6ca65e0c3c4a5ac978c9179fd90e12bf34))
